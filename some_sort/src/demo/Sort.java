package demo;

public class Sort {

	public static void main(String[] args) {
		Comparable[] array = new Comparable[5];
		
		array[0] = new MyInt(10);
		array[1] = new MyInt(7);
		array[2] = new MyInt(9);
		array[3] = new MyInt(1);
		array[4] = new MyInt(13);

		sort(array);
		
		for (int i=0; i<array.length;i++) {
			MyInt tmp = (MyInt) array[i];
			System.out.println(tmp.getValue());
		}
	}

	private static void sort(Comparable[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			boolean swapped = false;
			for (int j = 0; j < arr.length - i - 1;j++) {
				if (arr[j].isLess(arr[j + 1])) {
					Comparable tmp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = tmp;
					swapped = true;
				}
			}
			if (!swapped) break;
		}
	}
}
