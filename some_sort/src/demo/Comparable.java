package demo;

public interface Comparable {

	boolean isGreater(Object x);
	boolean isLess(Object x);
	boolean isEqual(Object x);
}
