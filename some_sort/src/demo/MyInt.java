package demo;

public class MyInt implements Comparable {
	private int value;

	public MyInt(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public boolean isGreater(Object x) {
		MyInt value = (MyInt) x;
		return value.getValue() > this.value;
	}

	@Override
	public boolean isLess(Object x) {
		MyInt value = (MyInt) x;
		return value.getValue() < this.value;
	}

	@Override
	public boolean isEqual(Object x) {
		MyInt value = (MyInt) x;
		return value.getValue() == this.value;
	}
	
	
	

}
