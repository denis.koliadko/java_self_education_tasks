
public abstract class Test_Deck {

	public static void main(String[] args) {

		Deck deck = new Deck();
		
		System.out.println("Test");
		
		deck.printDeck(deck.getCardArray());
		System.out.println("DeckSize is:" + deck.getDeckSize());
		
		deck.shuffle();
		deck.printDeck(deck.getCardArray());
		System.out.println("DeckSize is:" + deck.getDeckSize());
		
		for (int i = 0; i < deck.getCardArray().length - 3; i++) {
			Card card = deck.drawOne();
			System.out.printf("Draw Card is: %s %s\n", card.getSuit().getName(), card.getRank().getName());
			System.out.printf("DeckSize is: %d\n", deck.getDeckSize());
			System.out.println(deck.hasNext());
		}
		
		deck.printDeck(deck.getCardArray());
		deck.order();
		
		System.out.println(deck.getCardArray().length);
		deck.printDeck(deck.getCardArray());

	}

}
