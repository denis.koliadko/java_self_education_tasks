public class Rank {
	private static final Rank ACE = new Rank("Ace");
	private static final Rank KING = new Rank("King");
	private static final Rank QUEEN = new Rank("Queen");
	private static final Rank JACK = new Rank("Jack");
	private static final Rank TEN = new Rank("10");
	private static final Rank NINE = new Rank("9");
	private static final Rank EIGHT = new Rank("8");
	private static final Rank SEVEN = new Rank("7");
	private static final Rank SIX = new Rank("6");

	public static Rank[] values = { ACE, KING, QUEEN, JACK, TEN, NINE, EIGHT, SEVEN, SIX };

	private String name;

	private Rank(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
