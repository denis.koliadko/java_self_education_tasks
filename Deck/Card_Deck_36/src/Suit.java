
public class Suit {
	private static final Suit HEARTS = new Suit("HEARTS");
	private static final Suit DIAMONDS = new Suit("DIAMONDS");
	private static final Suit CLUBS = new Suit("CLUBS");
	private static final Suit SPADES = new Suit("SPADES");
	
	public static Suit[] values = { HEARTS, DIAMONDS, CLUBS, SPADES};
	
	private String name;

	private Suit(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
