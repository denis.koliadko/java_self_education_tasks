
public class Deck {	
	private Card[] cardArray;
	private int deckSize;
	
    public Deck() {    	
    	this.cardArray = new Card[Suit.values.length * Rank.values.length];
    	int count = 0;
    	
    	for (int i = 0; i < Suit.values.length; i++) {
    		for (int j = 0; j < Rank.values.length; j++) {
    			Card card = new Card(Rank.values[j], Suit.values[i]);    			
    			this.cardArray[count] = card;
    			count++;
    		}
    		this.deckSize = count;
    	} 
    }  

    // Mix in random order
    public void shuffle() {    	
    	int b = this.deckSize;
    	
		for (int i = 0; i < this.cardArray.length; i++) {
			int random1 = (int) (Math.random() * b);
			int random2 = (int) (Math.random() * b);
			Card tmp = this.cardArray[random1];
			this.cardArray[random1] = this.cardArray[random2];
			this.cardArray[random2] = tmp;
		}
    }
    /* Orders deck
    * Sorting order:
    * First, all cards with suit HEARTS, then DIAMONDS, CLUBS, SPADES
    * for each suit the order is as follows: Ace, King, Queen, Jack, 10,9,8,7,6
    * Example:
    * HEARTS Ace 
    * HEARTS King 
    * HEARTS Queen 
    * HEARTS Jack 
    * HEARTS 10 
    * HEARTS 9 
    * HEARTS 8 
    * HEARTS 7 
    * HEARTS 6
    * etc
    */
    public void order() {
    	Card temp;
    	for( int i = 0; i < this.deckSize; i++) {
    	    for( int j = this.deckSize-1; j > i; j--) {
    	    	String st1 = this.cardArray[j - 1].getSuit().getName();
    	    	String st2 = this.cardArray[j - 1].getRank().getName();
    	       	int index1 = getCardIndex(st1, st2);

    	       	st1 = this.cardArray[j].getSuit().getName();
    	    	st2 = this.cardArray[j].getRank().getName();
    	       	int index2 = getCardIndex(st1, st2);
    	    	
    	       	if (index1 > index2) {
    	       		temp = this.cardArray[j];
    	       		this.cardArray[j] = this.cardArray[j - 1];
    	       		this.cardArray[j - 1]=temp;
    	       	}
    	    }
    	}	
    }
    
    // Returns true if there are cards in the deck
    public boolean hasNext() { 
    	return (this.deckSize > 0);
    }
    
    // Removes one card from the deck, when all 36 cards are returned, returns null
    // The cards are taken out of the "top" of the deck. For example, the first call will be  SPADES 6 then
    // SPADES 7, ..., CLUBS 6, ..., CLUBS Ace and so on to HEARTS Ace
    public Card drawOne() {
    	int x = (this.deckSize - 1);
    	if (x < 0) {
    	    return null;
    	}
    	else {
            Card tmp = this.cardArray[x];
            this.deckSize--;
            this.cardArray[x] = null;
            return tmp;
    	}
    }
    
    public int getCardIndex (String suit, String rank) {
    	int a = 0;
    	int b = 0;
    	
    	if (suit.equals("HEARTS")) a = 0;
    	if (suit.equals("DIAMONDS")) a = 1;
    	if (suit.equals("CLUBS")) a = 2;
    	if (suit.equals("SPADES")) a = 3;
    	    	
    	if (rank.equals("Ace")) b = 0;
    	if (rank.equals("King")) b = 1;
    	if (rank.equals("Queen")) b = 2;
    	if (rank.equals("Jack")) b = 3;
    	if (rank.equals("10")) b = 4;
    	if (rank.equals("9")) b = 5;
    	if (rank.equals("8")) b = 6;
    	if (rank.equals("7")) b = 7;
    	if (rank.equals("6")) b = 8;
    		    	
    	return ((a * 9) + b);
    }
    
    public void printDeck (Card[] cardarray) {
    	int count = this.deckSize;
    	if ((cardarray==null)||(count<=0)) System.out.println("Deck is empty");
    	else {
    		 System.out.println("Deck is not empty");
    		 for (int i = 0; i < count; i++) {
    			 System.out.printf("Suit: %s; Rank is: %s\n",
						 cardarray[i].getSuit().getName(),
						 cardarray[i].getRank().getName());
    		 }
    	}
    }

    public Card[] getCardArray() {
        return cardArray;
    }

    public int getDeckSize() {
        return deckSize;
    }
}

