package solution;

public class Person {
	private String firstName;
	private String lastName;
	private int idNumber;
	
	public Person(String firstName, String lastName, int identification) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.idNumber = identification;
	}
	
	public void printPerson() {
		System.out.printf("Name: %s, %s\nID: %d\n", lastName, firstName, idNumber);
	}
}
