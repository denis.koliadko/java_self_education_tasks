package solution;

import java.util.Arrays;
import java.util.Scanner;

public class Solution {
	
	int[] testScores = new int[] {41, 42, 1, 44, 45, 46, 48};
	
	int maximumDifference = 0;
	
	void computeDifference() {
        int[] tempArray = testScores;
        Arrays.sort(tempArray);
        maximumDifference = tempArray[tempArray.length - 1] - tempArray[0];
        System.out.printf("maximumDifference is %d\n", maximumDifference);
    }
	
    public static void main(String[] args) {
    	
    	Solution solution = new Solution();

		solution.computeDifference();
    	
    	for (int item : solution.testScores) {
    		System.out.printf("%d \n", item);
    	}

    	Scanner scan = new Scanner(System.in);

		System.out.println("Name of the student:");
		String firstName = scan.next();
		System.out.println("Surmname of the student:");
    	String lastName = scan.next();
		System.out.println("His ID:");
    	int id = scan.nextInt();
		System.out.println("number of scores:");
    	int numScores = scan.nextInt();

    	int[] testScores = new int[numScores];
    	System.out.printf("Enter %d scores:\n", numScores);
    	for (int i = 0; i < numScores; i++) {
    		testScores[i] = scan.nextInt();
    	}

		//Student st = new Student ("Sancho", "Panza", 4847677, testScores);
    	Student st = new Student (firstName, lastName, id, testScores);
    	st.printPerson();
    	System.out.println(st.calculate());
    }
}