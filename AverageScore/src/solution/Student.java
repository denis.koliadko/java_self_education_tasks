package solution;

public class Student extends Person {
	private int[] testScores;

	public Student (String firstName, String lastName, int id, int[] scores) {
		super(firstName, lastName, id);
		this.testScores = scores;
	}

	public char calculate() {
		double average = 0;
		char rate;
		for (int i = 0; i < testScores.length; i++) {
			average+=testScores[i];
		}
		average/=testScores.length;
		System.out.println(average);
		if (average < 40) rate = 'T';
		else if (average < 55) rate = 'D';
			else if (average < 70) rate = 'P';
				else if (average < 80) rate = 'A';
			 		else if (average < 90) rate = 'E';
			 			else rate = 'O';
		return rate;
	}
}
