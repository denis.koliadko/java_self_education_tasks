package simple_numbers;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input your int number (greater than 2), please:");
        String intS = scanner.next();
        int numberI = 0;
        try {
            numberI = Integer.parseInt(intS);
        } catch (NumberFormatException e) {
            System.out.println("Invalid data");
            System.exit(1);
        }
        if (numberI < 2) {
            System.out.println("Invalid data");
            System.exit(1);
        }
        if (numberI == 2) {
            System.out.println("The only simple number is 2");
            System.exit(0);
        }

        System.out.printf("simple number's found: %d\n", 2);
        for (int i = 3; i <= numberI; i = i + 2) {
            if (isSimple(i)) {
                System.out.printf("simple number's found: %d\n", i);
            }
        }
    }

    private static boolean isSimple(int num) {
        for (int i = 3; i < num; i = i + 2) {
            if ((num % i) == 0) return false;
        }
        return true;
    }
}
