package demo_convert;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input your weight, please:");
        String weight = scanner.next();
        double weightD = 0;
        try {
            weightD = Double.parseDouble(weight);
        } catch (NumberFormatException e) {
            System.out.println("Invalid data");
            System.exit(1);
        }
        System.out.printf("Your weight on the moon is: %.2f kg\n", weightD * 17 / 100);

        System.out.println("Input your height (in inches), please:");
        String height = scanner.next();
        double heightD = 0;
        try {
            heightD = Double.parseDouble(height);
        } catch (NumberFormatException e) {
            System.out.println("Invalid data");
            System.exit(1);
        }
        System.out.printf("Your height in Europe is: %.2f m", heightD / 39.37f);
    }
}
