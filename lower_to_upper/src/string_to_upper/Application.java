package string_to_upper;

import java.io.*;

public class Application {

    public static void main(String[] args) throws IOException {
        char ch;
        int counter = 0;
        StringBuilder result = new StringBuilder();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in) );

        System.out.println("Input some string. '.' means the end of input\n");
        do {
            ch = (char) br.read();
            if (ch >=  'a' && ch <= 'z') {
                ch -= 32;
                counter++;
            } else if (ch >=  'A' && ch <= 'Z') {
                ch += 32;
                counter++;
            }
            result.append(ch);
        } while(ch != '.');

        br.close();
        System.out.printf("Transformed string: %s\n", result.toString());
        System.out.printf("Number of changes is: %d\n", counter);
    }
}