package com.apriorit.shildt.testAppCh12;

public class TrafficLightDemo {
    public static void main(String[] args) {

        TrafficLightSimulator trafficLightSimulator = new TrafficLightSimulator(TrafficLightColor.GREEN);

        for (int i = 0; i < 9; i++) {
            System.out.println(trafficLightSimulator.getColor() + " for " + trafficLightSimulator.getColor().getSleep() / 1000 + " sec");
            trafficLightSimulator.waitForChange();
        }
        trafficLightSimulator.cancel();
    }
}

class TrafficLightSimulator implements Runnable {

    private Thread thrd;
    private TrafficLightColor currentLightColor;

    boolean stop = false; // stop the light if true
    boolean changed = false; // switch the light if true

    TrafficLightSimulator(TrafficLightColor initColor) {
        currentLightColor = initColor;
        thrd = new Thread(this);
        thrd.start();
        System.out.println("turned on");
    }

    TrafficLightSimulator() {
        currentLightColor = TrafficLightColor.RED;
        thrd = new Thread(this);
        thrd.start();
        System.out.println("turned on");
    }

    @Override
    public void run() {
        try {
            while (!stop) {
                Thread.sleep(currentLightColor.getSleep());
                changeColor();
            }
        } catch (InterruptedException e) {
            System.out.println(e);
        }

//        try {
//            while (!stop) {
//                switch (currentLightColor) {
//                    case GREEN:
//                        Thread.sleep(2000);
//                        break;
//                    case YELLOW:
//                        Thread.sleep(1000);
//                        break;
//                    case RED:
//                        Thread.sleep(3000);
//                        break;
//                }
//                changeColor();
//            }
//        } catch (InterruptedException e) {
//            System.out.println(e);
//        }
    }

    private synchronized void changeColor() {
        switch (currentLightColor) {
            case RED:
                currentLightColor = TrafficLightColor.GREEN;
                break;
            case YELLOW:
                currentLightColor = TrafficLightColor.RED;
                break;
            case GREEN:
                currentLightColor = TrafficLightColor.YELLOW;
                break;
        }
        changed = true;
        notify();
    }

    synchronized void waitForChange() {
        try {
            while (!changed) {
                wait();
            }
            changed = false;
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }

    TrafficLightColor getColor() {
        return currentLightColor;
    }

    void cancel() {
        stop = true;
        System.out.println("turned off");
    }
}

enum TrafficLightColor {
    RED(3000), GREEN(2000), YELLOW(1000);

    int sleep;

    TrafficLightColor(int value) {
        sleep = value;
    }

    int getSleep() {
        return sleep;
    }
}


// Пример использования аннотации @Deprecated.

// Пометить класс как не рекомендованный к применению.
@Deprecated
class MyClass {
    private String msg;

    MyClass(String m) {
        msg = m;
    }

    // Пометить метод в классе как
    // не рекомендованный к применению.

    @Deprecated
    public String getMsg() {
        return msg;
    }

    // ...
}

class AnnoDemo {
    public static void main(String args[]) {
        MyClass myObj = new MyClass("test");

        System.out.println(myObj.getMsg());
    }
}