package com.apriorit.shildt.testAppCh12;

public class EnumDemo {
    public static void main(String[] args) {
        Transport tp = Transport.AIRPLANE;

        System.out.println("Typical speed for an airplane is " + tp.getSpeed() + " miles per hour.\n");
        System.out.println("All Transport speeds: ");
        for (Transport t : Transport.values()) {
            System.out.println(t + " typical speed is " + t.getSpeed() + " miles per hour.");
        }
    }
}

// Применение конструктора, переменной экземпляра и
// метода в перечислении,
enum Transport {

    CAR(65), TRUCK(55), AIRPLANE(600), TRAIN(70), BOAT(22);

    private int speed;

    Transport(int s) {
        speed = s;
    }

    int getSpeed() {
        return speed;
    }
}