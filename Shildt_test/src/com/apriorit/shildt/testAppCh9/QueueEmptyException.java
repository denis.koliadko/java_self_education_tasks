package com.apriorit.shildt.testAppCh9;

// Исключение, указывающее на опустошение очереди,
public class QueueEmptyException extends Exception {
    public String toString()    {
        return "\nQueue is empty.";
    }
}

