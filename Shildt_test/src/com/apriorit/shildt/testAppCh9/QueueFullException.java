package com.apriorit.shildt.testAppCh9;

/*
Пример для опробования 9.1.
Добавление обработчиков исключений в класс очереди.
*/
// Исключение, указывающее на переполнение очереди,
public class QueueFullException extends Exception {
    int size;

    public QueueFullException(int s) {
        size = s;
    }

    public String toString()    {
        return "\nQueue is full. Maximum size is " + size;
    }
}
