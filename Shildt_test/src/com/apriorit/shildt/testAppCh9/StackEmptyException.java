package com.apriorit.shildt.testAppCh9;

public class StackEmptyException extends Exception {
    public String toString() {
        return "\nStack is empty.";
    }
}
