package com.apriorit.shildt.testAppCh9;

class NonlntResultException extends Exception {
    int num1, num2;

    public NonlntResultException(int i, int i1) {
        num1 = i;
        num2 = i1;
    }

    @Override
    public String toString() {
        return "Result of " + num1 + " / " + num2 +
                " is non-integer.";
    }
}


public class CustomExceptDemo {
    public static void main(String[] args) {
        // В массиве numer содержатся нечетные числа,
        int numer[] = { 4, 8, 15, 32, 64, 127, 256, 512 };
        int denom[] = { 2, 0, 4, 4, 0, 8 };

        for (int i = 0; i < numer.length; i++)   {
            try {
                if ((numer[i] % 2) != 0) {
                    throw new NonlntResultException(numer[i], denom[i]);
                }
                System.out.println(numer[i] + " / " + denom[i] + " is " + numer[i] / denom[i]);
            }
            catch (ArithmeticException exc) {
                // перехватить исключение
                System.out.println("Cannot divide by zero");
            }
            catch (ArrayIndexOutOfBoundsException exc) {
                // перехватить исключение
                System.out.println("No matching element found.");
            }
            catch (NonlntResultException exc) {
                System.out.println(exc) ;
            }
        }
    }
}
