package com.apriorit.shildt.testAppCh14;

public class MethodRefDemo {

    static boolean numTest(IntPredicate p, int v) {
        return p.test(v);
    }

    public static void main(String[] args) {
        boolean result;

        result = numTest(MyIntPredicates::isPrime, 17);
        if (result) System.out.println("17 - prostoe chislo - prime number");

        result = numTest(MyIntPredicates::isEven, 17);
        if (!result) System.out.println("17 - nechetnoe chislo - noteven number");

        result = numTest(MyIntPredicates::isPositive, 17);
        if (result) System.out.println("17 - polozhitelnoe chislo - positive number");

        //--------------------------------------------------------------------------------------------------------------

        MyIntNum myIntNum1 = new MyIntNum(12);
        MyIntNum myIntNum2 = new MyIntNum(16);

        IntPredicate ip = myIntNum1::isFactor; // 12

        result = ip.test(3);
        if (result) System.out.println("3 - delitel for 12");

        ip = myIntNum2::isFactor; // 16
        result = ip.test(3);
        if (!result) System.out.println("3 - is not delitel for 16");

        MyIntNum myIntNum3 = new MyIntNum(12);
        ip = myIntNum3::hasCommonFactor;
        result = ip.test(9);
        if (result) System.out.println("common divider found");
    }
}

interface IntPredicate {
    boolean test(int i);
}

class MyIntNum {
    private int v;

    MyIntNum(int x) {
        v = x;
    }

    int getNum() {
        return v;
    }

    boolean isFactor(int n) {
        return ((v % n) == 0);
    }

    boolean hasCommonFactor(int n) {
        for (int i = 2; i < (v / i); i++) {
            if (((v % i) == 0) && ((n % i) == 0)) {
                return true;
            }
        }
        return false;
    }
}

class MyIntPredicates {

    static boolean isPrime(int n) {
        if (n < 2) return false;

        for (int i = 2; i <= n / i; i++) {
            if ((n % i) == 0) {
                return false;
            }
        }
        return true;
    }

    static boolean isEven(int n) {
        return ((n % 2) == 0);
    }

    static boolean isPositive(int n) {
        return n > 0;
    }
}
