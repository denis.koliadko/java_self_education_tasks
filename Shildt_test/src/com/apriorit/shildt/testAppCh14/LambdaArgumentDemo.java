package com.apriorit.shildt.testAppCh14;

public class LambdaArgumentDemo {
    public static void main(String[] args) {
        String inStr = "lamda Expressions";
        String outStr;

        StringFunc reverse= str -> {
           String res = "";
           for (int i = str.length() - 1; i >= 0; i--) {
               res += str.charAt(i);
           }
           return res;
        };

        outStr = changeStr(reverse, inStr);
        System.out.println(outStr);

        outStr = changeStr(str -> str.replaceAll(" ", "-"), inStr);
        System.out.println(outStr);

        outStr = changeStr(str -> {
            String res= "";
            char ch;

            for (int i = 0; i < str.length(); i++) {
                ch = str.charAt(i);
                if (Character.isUpperCase(ch)) {
                    res += Character.toLowerCase(ch);
                } else {
                    res += Character.toUpperCase(ch);
                }
            }
            return str;}, inStr);
        System.out.println(outStr);
    }

    private static String changeStr(StringFunc sf, String s) {
        return sf.func(s);
    }
}

interface StringFunc {
    String func(String str);
}