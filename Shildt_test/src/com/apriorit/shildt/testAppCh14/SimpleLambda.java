package com.apriorit.shildt.testAppCh14;

public class SimpleLambda {
    public static void main(String[] args) {
        MyDVal d = new MyDVal()  {
            @Override
            public double getValue() {
                return 0.5;
            }
        };

        MyDVal d1 = () -> 0.5;

        System.out.println("traditional: " + d.getValue() + " " + d.getValueDefault());
        System.out.println("usin lambda: " + d1.getValue() + " " + d1.getValueDefault());

        double test = 10.0;

        MyDValParam d2 = (n) -> test / n; // переменной n присваивается тип исходя из контекста

        System.out.println(d2.getValue(2));
        System.out.println(d2.getValue(1));
    }
}

interface MyDVal {   // функциональный интерфейс
    double getValue();

    default double getValueDefault() {  // с пом default даем реализацию методу
        return getValue();
    }
}

interface MyDValParam {
    double getValue(double d);
}