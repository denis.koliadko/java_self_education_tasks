package com.apriorit.shildt.testAppCh14;

public class NumTestLambda {
    public static void main(String[] args) {
        NumericTest isfactor = (arg1, arg2) -> (arg1 % arg2) == 0;
        NumericTest lessthan = (arg1, arg2) -> (arg1 < arg2);
        NumericTest morethan = (arg1, arg2) -> (arg1 > arg2);
        NumericTest absEqual = (arg1, arg2) -> (arg1 < 0 ? -arg1 : arg1) == ( arg2 < 0 ? -arg2 : arg2);

        System.out.println("isfactor 10 / 2 " + isfactor.test( 10, 2));
        System.out.println("10 less than 2 " + lessthan.test(10,2));
        System.out.println("10 more than 2 " + morethan.test(10,2));
        System.out.println("10 absequal 2 " + absEqual.test(10, 2));
        System.out.println("-2 absequal 2 " + absEqual.test(-2, 2));

        NumericFunc smallestDivider = (n) -> {
            int result = 1;
            n = (n < 0) ? -n : n;  // get abs of n

            for (int i = 2; i <= n; i++) {
                if ((n % i) == 0) {
                    result = i;
                    break;
                }
            }
            return result;
        };

        System.out.println("наименьший делитель для 11 " + smallestDivider.func(11)); // 11
        System.out.println("наименьший делитель для 12 " + smallestDivider.func(12)); // 12
    }
}

interface NumericTest {
    boolean test(int i, int j);
}

interface NumericFunc {
    int func(int i);
}
