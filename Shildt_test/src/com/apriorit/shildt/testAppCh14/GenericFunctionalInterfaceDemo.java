package com.apriorit.shildt.testAppCh14;

public class GenericFunctionalInterfaceDemo {
    public static void main(String[] args) {
        SomeTest<Integer> isFactor = (n, d) -> (n % d) == 0;
        if (isFactor.test(10, 2)) System.out.println("2 is factor for 10\n");

        SomeTest<Double> isFactorD = (n, d) -> (n % d) == 0;
        if (isFactorD.test(212.0, 2.0)) System.out.println("2.0 is factor for 212.0\n");

        String testStr = "functional interface";

        SomeTest<String> isIn = (n, d) -> n.indexOf(d) != -1;
        if (isIn.test(testStr, "face")) {
            System.out.println("found\n");
        } else {
            System.out.println("not found\n");
        }
    }
}

interface SomeTest<T> {
    boolean test(T n, T m);
}