package com.apriorit.shildt.testAppCh16;

/*
    Пример для опробования 15-1.

    Утилита сравнения файлов, создаваемая на основе Swing.

    Для компиляции этой утилиты требуется JDK 7
    или более поздняя версия данного комплекта.
*/

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;

class FileCompsreSwingDemo implements ActionListener {

    private JTextField jtfFirst; // Переменная для хранения имени первого файла
    private JTextField jtfSecond; // Переменная для хранения имени второго файла

    private JLabel jlabResult; // Сведения о результатах и сообщения об ошибках
    JCheckBox jcbLoc; // флажок для показа места первого несовпадения файлов

    private FileCompsreSwingDemo() {

        // создать новый контейнер JFrame
        JFrame jfrm = new JFrame("Compare Files");

        // установить диспетчер компоновки FlowLayout
        jfrm.setLayout(new FlowLayout());

        // задать исходные размеры рамки окна
        jfrm.setSize(200, 190);

        // завершить программу после закрытия окна
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // создать поля для ввода имен файлов
        jtfFirst = new JTextField(14);
        jtfSecond = new JTextField(14);

        // установить команды действия для полей ввода текста
        jtfFirst.setActionCommand("fileA");
        jtfSecond.setActionCommand("fileB");

        // создать кнопку Compare
        JButton jbtnComp = new JButton("Compare");

        // добавить приемник событий действия от кнопки Compare
        jbtnComp.addActionListener(this);

        // создать метки
        JLabel jlabFirst = new JLabel("First file: ");
        JLabel jlabSecond = new JLabel("Second file: ");
        jlabResult = new JLabel("");
        // создать флажок
        jcbLoc = new JCheckBox("Show position of mismatch");
        // добавить компоненты на панели содержимого
        jfrm.add(jlabFirst);
        jfrm.add(jtfFirst);
        jfrm.add(jlabSecond);
        jfrm.add(jtfSecond);
        jfrm.add(jbtnComp);
        jfrm.add(jlabResult);
        jfrm.add(jcbLoc);
        // отобразить рамку окна
        jfrm.setVisible(true);
    }

    // сравнить файлы после нажатия кнопки Compare
    public void actionPerformed(ActionEvent ae) {
        int i, j;

        // сначала убедиться, что введены имена обоих файлов
        if (jtfFirst.getText().equals("")) {
            jlabResult.setText("First file name missing.");
            return;
        }
        if (jtfSecond.getText().equals("")) {
            jlabResult.setText("Second file name missing.");
            return;
        }

        // сравнить файлы, используя оператор try с ресурсами
        try (FileInputStream f1 = new FileInputStream(jtfFirst.getText());

             FileInputStream f2 = new FileInputStream(jtfSecond.getText())) {
            // проверить содержимое каждого файла
            int count = 0;
            do {
                count++;
                i = f1.read();
                j = f2.read();
                if (i != j) break;
            } while (i != -1);
            if (i != j) {
                if (jcbLoc.isSelected())
                    jlabResult.setText("Files differ at location " + count);
                else
                    jlabResult.setText("Files are not the same.");
            } else
                jlabResult.setText("Files compare equal.");
        } catch (IOException exc) {
            jlabResult.setText("File Error");
        }
    }

    public static void main(String args[]) {
        // создать рамку окна в потоке диспетчеризации событий
        SwingUtilities.invokeLater(FileCompsreSwingDemo::new);
    }
}
