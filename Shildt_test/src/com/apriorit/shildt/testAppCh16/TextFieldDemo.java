package com.apriorit.shildt.testAppCh16;

// Применение поля ввода текста.

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class TextFieldDemo implements ActionListener {

    private JTextField jtf;
    private JLabel jlabContents;

    private TextFieldDemo() {

        // создать новый контейнер JFrame
        JFrame jfrm = new JFrame("Use a Text Field");

        // установить диспетчер компоновки FlowLayout
        jfrm.setLayout(new FlowLayout());

        // задать исходные размеры рамки окна
        jfrm.setSize(240, 120);

        // завершить программу после закрытия окна
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Создание поля ввода текста шириной 10 символов.
        jtf = new JTextField(10);

        // Установка команды действия для поля ввода текста.
        jtf.setActionCommand("myTF");

        // создать кнопку Reverse
        JButton jbtnRev = new JButton("Reverse");

        // Добавление приемников событий от поля ввода и кнопки.
        jtf.addActionListener(this);
        jbtnRev.addActionListener(this);

        // создать метки
        JLabel jlabPrompt = new JLabel("Enter text: ");
        jlabContents = new JLabel("");

        // добавить компоненты на панели содержимого
        jfrm.add(jlabPrompt);
        jfrm.add(jtf);
        jfrm.add(jbtnRev);
        jfrm.add(jlabContents);

        // отобразить рамку окна
        jfrm.setVisible(true);
    }

    // Обработка событий от кнопки и поля ввода текста.
    public void actionPerformed(ActionEvent ae) {
        // Для определения компонента, сформировавшего событие,
        // используется команда действия.
        switch (ae.getActionCommand()) {
            case "Reverse":
                // Нажатие кнопки Reverse.
                String orgStr = jtf.getText();
                StringBuilder resStr = new StringBuilder();

                // обратить символьную строку в поле ввода текста
                for (int i = orgStr.length() - 1; i >= 0; i--)
                    resStr.append(orgStr.charAt(i));

                // сохранить обращенную строку в поле ввода текста
                jtf.setText(resStr.toString());
                break;
            case "myTF":
                // Нажатие клавиши <Enter> в тот момент, когда фокус
                // ввода находится в поле ввода текста,
                jlabContents.setText("You pressed ENTER. Text is: " + jtf.getText());
                break;
            default:
                jlabContents.setText("");
                break;
        }
    }

    public static void main(String args[]) {
        // создать рамку окна в потоке диспетчеризации событий
        SwingUtilities.invokeLater(TextFieldDemo::new);
    }
}