package com.apriorit.shildt.testAppCh16;

// Демонстрация нажатия кнопки и обработки событий действия.

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class ButtonDemo implements ActionListener {
    private JLabel jlab;

    private ButtonDemo() {

        // создать новый контейнер JFrame
        JFrame jfrm = new JFrame("A Button Example");

        // установить диспетчер компоновки FlowLayout
        jfrm.setLayout(new FlowLayout());

        // задать исходные размеры рамки окна   <
        jfrm.setSize(220, 90);

        // завершить программу после закрытия окна
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Создание двух кнопок.
        JButton jbtnUp = new JButton("Up");
        JButton jbtnDown = new JButton("Down");

        // Добавление приемников событий от кнопки.
        jbtnUp.addActionListener(this);
        jbtnDown.addActionListener(this);

        // Добавление кнопок на панели содержимого.
        jfrm.add(jbtnUp);
        jfrm.add(jbtnDown);

        // создать метку
        jlab = new JLabel("Press a button.");

        // добавить метку в рамке окна
        jfrm.add(jlab);

        // отобразить рамку окна
        jfrm.setVisible(true);
    }

    // Обработка событий от кнопки.
    public void actionPerformed(ActionEvent ae) {
        // Для определения нажатой кнопки используется команда действия.
        if (ae.getActionCommand().equals("Up"))
            jlab.setText("You pressed Up.");
        else
            jlab.setText("You pressed down. ");
    }

    public static void main(String args[]) {
        // создать рамку окна в потоке диспетчеризации событий
        SwingUtilities.invokeLater(ButtonDemo::new);
    }
}