package com.apriorit.shildt.testAppCh16;

// Демонстрация простого компонента JList.

// Для компиляции этой программы требуется JDK 7
// или более поздняя версия данного комплекта.

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;

class ListDemo implements ListSelectionListener {

    private JList<String> jlst;
    private JLabel jlab;

    // создать массив имен
    private String names[] = {"Sherry", "Jon", "Rachel", // Этот массив имен
            "Sasha", "Josselyn", "Randy", // будет отображаться
            "Tom", "Mary", "Ken", // списком в компоненте JList.
            "Andrew", "Matt", "Todd"};

    private ListDemo() {
        // создать новый контейнер JFrame
        JFrame jfrm = new JFrame("JList Demo");

        // установить диспетчер компоновки FlowLayout
        jfrm.setLayout(new FlowLayout());

        // задать исходные размеры рамки окна
        jfrm.setSize(200, 160);

        // завершить программу после закрытия окна
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // создать компонент JList
        jlst = new JList<>(names);    // Создание списка  имен.

        // задать режим выбора элементов    из списка
        // Переход в режим выбора элементов из списка по одному.
        //jlst.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // добавить список на панели прокрутки.
        // Компонент списка помещается в контейнер панели прокрутки.
        JScrollPane jscrlp = new JScrollPane(jlst);

        // задать предпочтительные размеры панели прокрутки
        jscrlp.setPreferredSize(new Dimension(120, 90));

        // создать метку для отображения    результатов выбора  из  списка,
        jlab = new JLabel("Please choose a name");

        // добавить обработчик событий, связанных с выбором из списка
        // Прием событий, наступающих при выборе элементов из списка.
        jlst.addListSelectionListener(this);

        // добавить список и метку на панели содержимого
        jfrm.add(jscrlp);
        jfrm.add(jlab);

        // отобразить рамку окна
        jfrm.setVisible(true);
    }

//    // обработать события, связанные с выбором элементов из списка
//    public void valueChanged(ListSelectionEvent le) {
//        // получить индекс того элемента, выбор которого был сделан
//        // или отменен в списке
//        int idx = jlst.getSelectedIndex();
//
//        // отобразить результат выбора, если элемент был выбран
//        if (idx != -1)
//            jlab.setText("Current selection: " + names[idx]);
//        else // иначе еще раз предложить сделать выбор
//            jlab.setText("Please choose a name");
//    }

    // обработать события, связанные с выбором элементов из списка
    public void valueChanged(ListSelectionEvent le) {
        // получить индексы тех элементов, выбор которых был сделан
        // или отменен в списке
        int indices[] = jlst.getSelectedIndices();
        // отобразить результат выбора, если был выбран один
        // или несколько элементов из списка
        if (indices.length != 0) {
            StringBuilder who = new StringBuilder();
            // построить символьную строку из выбранных имен
            for (int i : indices)
                who.append(names[i]).append(" ");
            jlab.setText("Current selections: " + who);
        } else // иначе еще раз предложить сделать выбор
            jlab.setText("Please choose a name");
    }

    public static void main(String args[]) {
        // создать рамку окна в потоке диспетчеризации событий
        SwingUtilities.invokeLater(ListDemo::new);
    }
}