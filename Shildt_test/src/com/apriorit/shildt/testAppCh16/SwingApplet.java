package com.apriorit.shildt.testAppCh16;

// Простой Swing-апплет.

import javax.swing.*;
import java.awt.*;

/*
    Этот код HTML может быть использован для загрузки апплета:
    <object code="MySwingApplet" width=200 height=80>
    </object>
*/

// Swing-апплет должен расширять класс JApplet.
public class SwingApplet extends JApplet {

    private JLabel jlab;

    // инициализировать апплет
    public void init() {
        try {
            // Для создания графического пользовательского интерфейса
            // апплета используется метод invokeAndWait().
            // инициализировать графический интерфейс
            SwingUtilities.invokeAndWait(this::makeGUI);
        } catch(Exception exc) {
            System.out.println("Can't create because of "+ exc);
        }
    }

    // В этом апплете нет нужды переопределять
    // методы start(), stop() и destroy().

    // установить и инициализировать графический интерфейс
    private void makeGUI() {
        // установить диспетчер компоновки FlowLayout для апплета
        setLayout(new FlowLayout());

        // создать две кнопки
        JButton jbtnUp = new JButton("Up");
        JButton jbtnDown = new JButton("Down");

        // добавить приемник событий от кнопки Up
        // Для обработки событий от кнопки Up
        // используется анонимный внутренний класс.
        jbtnUp.addActionListener(ae -> jlab.setText("You pressed Up."));

        // добавить приемник событий от кнопки Down
        // Для обработки событий от кнопки Down
        // используется анонимный внутренний класс.
        jbtnDown.addActionListener(ae -> jlab.setText("You pressed down."));

        // добавить кнопки на панели содержимого
        add(jbtnUp);
        add(jbtnDown);

        // создать текстовую метку
        jlab = new JLabel("Press a button.");

        // добавить метку на панели содержимого
        add(jlab);
    }
}