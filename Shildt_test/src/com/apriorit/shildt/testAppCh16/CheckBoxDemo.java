package com.apriorit.shildt.testAppCh16;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

class CheckBoxDemo implements ItemListener {
    private JLabel jlabSelected;
    private JLabel jlabChanged;
    private JCheckBox jcbAlpha;
    private JCheckBox jcbBeta;
    private JCheckBox jcbGamma;

    private CheckBoxDemo() {
        // создать новый контейнер JFrame
        JFrame jfrm = new JFrame("Demonstrate Check Boxes");

        // установить диспетчер компоновки FlowLayout
        jfrm.setLayout(new FlowLayout());

        // задать исходные размеры рамки окна
        jfrm.setSize(280, 120);

        // завершить программу после закрытия окна
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // создать пустые метки
        jlabSelected = new JLabel("");
        jlabChanged = new JLabel("");

        // Создание флажков.
        jcbAlpha = new JCheckBox("Alpha");
        jcbBeta = new JCheckBox("Beta");
        jcbGamma = new JCheckBox("Gamma");

        // События, формируемые компонентами JCheckBox, обрабатываются
        // одним методом itemStateChanged(), реализованным в классе CBDemo.
        jcbAlpha.addItemListener(this);
        jcbBeta.addItemListener(this);
        jcbGamma.addItemListener(this);

        // добавить флажки и метки на панели содержимого
        jfrm.add(jcbAlpha);
        jfrm.add(jcbBeta);
        jfrm.add(jcbGamma);
        jfrm.add(jlabChanged);
        jfrm.add(jlabSelected);

        // отобразить рамку окна
        jfrm.setVisible(true);
    }

    // Обработчик событий от элементов (в данном случае — флажков).
    public void itemStateChanged(ItemEvent ie) {
        String str = "";

        // Получение ссылки на компонент флажка, сформировавший событие.
        JCheckBox cb = (JCheckBox) ie.getItem();

        // сообщить об изменении состояния флажка
        if (cb.isSelected()) // Определение состояния флажка.
            jlabChanged.setText(cb.getText() + " was just selected.");
        else
            jlabChanged.setText(cb.getText() + " was just cleared.");

        // сообщить о всех установленных флажках
        if (jcbAlpha.isSelected()) {
            str += "Alpha ";
        }
        if (jcbBeta.isSelected()) {
            str += "Beta ";
        }
        if (jcbGamma.isSelected()) {
            str += "Gamma";
        }

        jlabSelected.setText("Selected check boxes: " + str);
    }

    public static void main(String args[]) {
        // создать рамку окна в потоке диспетчеризации событий
        SwingUtilities.invokeLater(CheckBoxDemo::new);
    }
}