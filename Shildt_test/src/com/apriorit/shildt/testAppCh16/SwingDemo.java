package com.apriorit.shildt.testAppCh16;

// Простая Swing-программа.

// Для каждой Swing-программы импортируется пакет javax.swing.

import javax.swing.*;
import java.awt.*;

class SwingDemo {

    private SwingDemo() {

        // Создание нового контейнера JFrame.
        JFrame jfrm = new JFrame("A Simple Swing Application");

        // Установка начальных размеров рамки окна.
        jfrm.setSize(275, 100);

        // При закрытии окна программа должна завершиться.
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Создание текстовой метки с помощью компонента Swing.
        JLabel jlab = new JLabel(" Swing defines the modern Java GUI.");
        BorderLayout bl = new BorderLayout();


        // Добавление метки на панели содержимого.
        jfrm.setLayout(bl);
        //jfrm.add(jlab, BorderLayout.CENTER);
        jfrm.add(jlab);

        // Отображение рамки окна.
        jfrm.setVisible(true);
    }

    public static void main(String args[]) {
        // Объект SwingDemo должен быть создан в потоке
        // диспетчеризации событий.
        SwingUtilities.invokeLater(SwingDemo::new);
    }
}