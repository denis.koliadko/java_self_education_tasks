package com.apriorit.shildt.testAppCh13;

//Stack
public interface IGenStack<T> {

    void put(T ch) throws Qexc.StackFullException;

    T get() throws Qexc.StackEmptyException;
}
