package com.apriorit.shildt.testAppCh13;

public class GenQueue<T> implements IGenQ<T> {

    private T[] q;   // Массив для хранения элементов очереди,
    private int putloc, getloc; // Индексы размещения и извлечения

    // создать пустую очередь заданного размера
    GenQueue(T[] arg) {
        q = arg;
        putloc = getloc = 0;
    }

    @Override
    public void put(T ch) throws Qexc.QueueFullException {
        if (putloc == q.length - 1) {
            throw new Qexc.QueueFullException(putloc);
        }
        putloc++;
        q[putloc] = ch;
    }

    @Override
    public T get() throws Qexc.QueueEmptyException {
        if (getloc == putloc) {
            throw new Qexc.QueueEmptyException();
        }
        getloc++;
        return q[getloc];
    }
}
