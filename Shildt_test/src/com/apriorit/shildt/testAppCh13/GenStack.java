package com.apriorit.shildt.testAppCh13;

public class GenStack<T> implements IGenStack<T> {

    private T[] stack;
    private int index;

    GenStack(T[] arg) {
        stack = arg;
        index = arg.length - 1;
    }

    @Override
    public void put(T ch) throws Qexc.StackFullException {
        if (index < 0) {
            throw new Qexc.StackFullException(stack.length);
        }
        stack[index] = ch;
        index--;
    }

    @Override
    public T get() throws Qexc.StackEmptyException {
        if (index == stack.length - 1) {
            throw new Qexc.StackEmptyException();
        }
        index++;
        return stack[index];
    }
}
