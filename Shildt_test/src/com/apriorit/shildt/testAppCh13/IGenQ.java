package com.apriorit.shildt.testAppCh13;

// Queue
public interface IGenQ<T> {

    void put(T ch) throws Qexc.QueueFullException;

    T get() throws Qexc.QueueEmptyException;
}