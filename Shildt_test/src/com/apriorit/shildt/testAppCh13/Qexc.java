package com.apriorit.shildt.testAppCh13;

class Qexc {
    static class QueueFullException extends Exception {
        int size;

        QueueFullException(int s) {
            size = s;
        }

        public String toString() {
            return "\nQueue is full. Maximum size is " +
                    size;
        }
    }

    static class QueueEmptyException extends Exception {
        public String toString() {
            return "\nQueue is empty.";
        }
    }

    static class StackFullException extends Exception {
        int size;

        StackFullException(int s) {
            size = s;
        }

        public String toString() {
            return "\nStack is full. Maximum size is " + size;
        }
    }

    static class StackEmptyException extends Exception {
        public String toString() {
            return "\nStack is empty.";
        }
    }


}
