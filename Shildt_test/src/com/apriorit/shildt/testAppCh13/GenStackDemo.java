package com.apriorit.shildt.testAppCh13;

public class GenStackDemo {
    public static void main(String args[]) {
        String[] sStore = new String[26];
        Character[] cStore = new Character[4];

        GenStack<String> bigS = new GenStack<>(sStore);
        GenStack<Character> smallS = new GenStack<>(cStore);

        String str;
        Character ch;
        int i;

        try {
            System.out.println("Using bigS to store the alphabet.");
            for (i = 0; i < 26; i++) {
                bigS.put("test" + i);
            }

            System.out.print("Contents of bigS: ");
            for (i = 0; i < 26; i++) { // empty stack exc
                str = bigS.get();
                System.out.print(str + " ");
            }
            System.out.println("\n");

            System.out.println("Using smallS to generate errors.");
            for (i = 0; i < 5; i++) {
                System.out.print("Attempting to store " + (char) ('Z' - i));
                smallS.put((char) ('Z' - i));
                System.out.println();
            }
            System.out.println();

            System.out.print("Contents of smallS: ");
            for (i = 0; i < 5; i++) {
                ch = smallS.get();
                if (ch != (char) 0) System.out.print(ch);
            }
        } catch (Qexc.StackFullException ex) {
            System.out.println(ex.toString());
        } catch (Qexc.StackEmptyException ex) {
            System.out.println(ex.toString());
        }
    }

}
