package com.apriorit.shildt.testAppCh13;

// Простой обобщенный класс.
// Здесь Т - это параметр типа, заменяемый именем
// подлинного типа при создании объекта класса Gen.

//В объявлении этого класса Т означает обобщенный тип.
class Gen<T, V extends Number> {
    private T obT; // объявить объект типа Т
    private V obV;
    // передать конструктору ссылку на объект типа Т.
    Gen (T ob1, V ob2) {
        obT = ob1;
        obV = ob2;
    }

    // возвратить объект ob из метода
    T getobT() {
        return obT;
    }

    V getobV() {
        return obV;
    }

    // отобразить тип Т
    void showTypes() {
        System.out.println("Type of T is " + obT.getClass().getName());
        System.out.println("Type of V is " + obV.getClass().getName());
    }
}

// продемонстрировать обобщенный класс
class GenDemo {
    public static void main(String args[]) {
        // Создание ссылки на объект типа Gen<Integer>.
        Gen<Integer, Double> iOb;

        // Создать объект типа Gen<Integer> и присвоить ссылку на
        // него переменной iOb. Обратите внимание на автоупаковку при
        // инкапсуляции значения 88 в объекте типа Integer.
        iOb = new Gen<>(88, 13.5); // получить экземпляр типа Gen<Integer>

        // отобразить тип данных, используемых в объекте iOb
        iOb.showTypes();

        // Получение значения из объекта iOb. Обратите внимание
        // на то,что приведение типов здесь не требуется,
        int v = iOb.getobT();
        System.out.println("value: " + v);
        System.out.println();

        double d = iOb.getobV();
        System.out.println("value: " + d);
        System.out.println();


        // Создание объекта типа Gen для символьных строк.
        // Здесь создается ссылка и сам объект типа Gen<String>.
        Gen<String, Integer> strOb = new Gen<>("Generics Test", 1);

        // отобразить тип данных, используемых в объекте strOb
        strOb.showTypes();

        // Получение значения из объекта strOb.
        //И здесь приведение типов не требуется.
        String str = strOb.getobT();
        System.out.println("value: " + str) ;
        int i = strOb.getobV();
        System.out.println("value: " + i) ;
    }
}