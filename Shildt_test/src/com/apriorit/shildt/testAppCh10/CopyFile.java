package com.apriorit.shildt.testAppCh10;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyFile {
    public static void main(String args[]) {
        int i;

// открыть оба файла для управления с помощью оператора try
        try (FileInputStream fin = new FileInputStream(args[0]);
             FileOutputStream fout = new FileOutputStream(args[1]))
        // Управление двумя ресурсами (в данном случае — файлами).
        {
            do {
                i = fin.read();
                if (i != -1) {
                    fout.write(i);
                }
            } while (i != -1) ;
        } catch(IOException exc) {
            System.out.println("I/O Error: " + exc);
        }
    }
}
