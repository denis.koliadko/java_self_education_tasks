package com.apriorit.shildt.testAppCh10;

/* Отображение текстового файла.
При вызове этой программы следует указать имя файла,
содержимое которого требуется просмотреть.
Например, для вывода на экран содержимого файла TEST.TXT,
в командной строке нужно указать следующее:
java ShowFile TEST.TXT
*/
import java.io.*;
class ShowFile {
    public static void main(String args[])
    {
        int i;
        FileInputStream fin = null;
// Прежде всего следует убедиться, что файл был указан,
        if (args.length < 1) {
            System.out.println("Usage: ShowFile FileOrig FileCopy");
            return;
        }
        try {
            fin = new FileInputStream(args[0]);
            do {
                i = fin.read();
                if(i != -1) System.out.print((char) i) ;
            } while(i != -1) ;
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        } catch(IOException exc) {
            System.out.println("Error Reading File");
            // Блок finally используется для закрытия файла.
        } finally {
            try {
                if (fin != null) fin.close();
            } catch(IOException exc) {
                System.out.println("Error Closing File");
            }
        }
        System.out.println("\nnow make a copy...");

        // Ниже оператор try с ресурсами применяется сначала для открытия, а
        // затем для автоматического закрытия файла после выхода из блока try.
        // открыть оба файла для управления с помощью оператора try
        try (FileInputStream fin2 = new FileInputStream(args[0]);
             FileOutputStream fout2 = new FileOutputStream(args[1]))
        // Управление двумя ресурсами (в данном случае — файлами).
        {
            do {
                i = fin2.read();
                if(i != -1) fout2.write(i);
            } while (i != -1) ;
        } catch(IOException exc) {
            System.out.println("I/O Error: " + exc);
        }
    }
}