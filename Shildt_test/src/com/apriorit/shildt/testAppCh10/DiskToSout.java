package com.apriorit.shildt.testAppCh10;

import java.io.*;

public class DiskToSout {
    public static void main(String args[]) {
        String s;
        try (BufferedReader br = new BufferedReader(new FileReader("d:\\test.txt"))) {
            while ((s = br.readLine()) != null) {
                System.out.println(s);
            }
        } catch (IOException exc) {
            System.out.println("I/O Error: " + exc);
        }
    }
}