package com.apriorit.shildt.testAppCh10;

import java.io.*;

public class CopyFile_replace {
    public static void main(String args[]) {
        int i;
        FileInputStream fin = null;
        FileOutputStream fout = null;

        if (args.length != 2) {
            System.out.println("Usage: Copy From file1 To file2");
            return;
        }

        try {
            fin = new FileInputStream(args[0]);
            fout = new FileOutputStream(args[1]);
            do {
                i = fin.read();
                if ((char) i == ' ') i = '-';
                if (i != -1) fout.write(i);
            } while (i != -1);
        } catch (IOException exc) {
            System.out.println("I/O Error: " + exc);
        } finally {
            try {
                if (fin != null) fin.close();
            } catch (IOException exc) {
                System.out.println("Error closing input file.");
            }
            try {
                if (fin != null) fout.close();
            } catch (IOException exc) {
                System.out.println("Error closing output file.");
            }
        }
    }
}