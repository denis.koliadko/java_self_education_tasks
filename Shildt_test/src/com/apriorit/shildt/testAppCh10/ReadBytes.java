package com.apriorit.shildt.testAppCh10;

import java.io.IOException;

public class ReadBytes {
    
    public static void main(String[] args) throws IOException {
        byte[] input = new byte[10];    
        
        System.out.println("Input something: ");
        System.in.read(input);
        System.out.println("You entered: ");
        for (byte c: input) {
            System.out.print((char) c);
        }
        System.out.println();

        int b;

        b = 'S';
        // Вывод байтов на экран.
        System.out.write(b);
        System.out.write('\n');
    }
}
