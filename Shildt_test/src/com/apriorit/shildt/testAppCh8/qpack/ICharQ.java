package com.apriorit.shildt.testAppCh8.qpack;

import com.apriorit.shildt.testAppCh9.QueueEmptyException;
import com.apriorit.shildt.testAppCh9.QueueFullException;

public interface ICharQ {
    void put(char c) throws QueueFullException;
    char get() throws QueueEmptyException;
}
