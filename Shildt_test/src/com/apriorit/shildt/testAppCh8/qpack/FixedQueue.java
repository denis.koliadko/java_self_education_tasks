package com.apriorit.shildt.testAppCh8.qpack;

import com.apriorit.shildt.testAppCh9.QueueEmptyException;
import com.apriorit.shildt.testAppCh9.QueueFullException;

public class FixedQueue implements ICharQ {
    private char q[];   // Массив для хранения элементов очереди,
    private int putloc, getloc; // Индексы размещения и извлечения

    // создать пустую очередь заданного размера
    public FixedQueue(int size) {
        q = new char[size + 1]; // выделить память для очереди
        putloc = getloc = 0;
    }

    // поместить символ в очередь
    public void put(char ch) throws QueueFullException {
        if (putloc == q.length - 1) {
            throw new QueueFullException(putloc);
        }
        putloc++;
        q[putloc] = ch;
    }

    // извлечь символ из очереди
    public char get() throws QueueEmptyException {
        if (getloc == putloc) {
            throw new QueueEmptyException();
        }
        getloc++;
        return q[getloc];
    }
}
