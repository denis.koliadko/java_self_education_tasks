package com.apriorit.shildt.testAppCh8.interf;

public interface Series {
    int getNext(); // возвратить следующее по порядку число
    void reset(); // начать все с самого сначала
    void setStart(int х); // задать начальное значение
}

