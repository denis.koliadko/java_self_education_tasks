package com.apriorit.shildt.testAppCh8.interf;

import com.apriorit.shildt.testAppCh8.qpack.CircularQueue;
import com.apriorit.shildt.testAppCh8.qpack.DynQueue;
import com.apriorit.shildt.testAppCh8.qpack.FixedQueue;
import com.apriorit.shildt.testAppCh8.qpack.ICharQ;
import com.apriorit.shildt.testAppCh9.QueueEmptyException;
import com.apriorit.shildt.testAppCh9.QueueFullException;

public class IQDemo {
    public static void main(String args[]) {
        FixedQueue q1 = new FixedQueue(10);
        DynQueue q2 = new DynQueue(5);
        CircularQueue q3 = new CircularQueue(10);

        ICharQ iQ;

        char ch;
        int i;

        iQ = q1;
        try {
            // поместить ряд символов в очередь фиксированного размера
            for (i = 0; i < 10; i++) {
                iQ.put((char) ('A' + i) ) ;
            }
            // отобразить содержимое очереди
            System.out.print("Contents of fixed queue: ");

            for (i = 0; i < 10; i++)   {
                ch = iQ.get() ;
                System.out.print(ch);
            }
            System.out.println();
            iQ = q2;
            // поместить ряд символов в динамическую очередь
            for (i = 0; i < 10; i++) {
                iQ.put((char) ('Z' - i));
            }
            // отобразить содержимое очереди
            System.out.print("Contents of dynamic queue: ");
            for (i = 0; i < 10; i++) {
                ch = iQ.get();
                System.out.print(ch);
            }
            System.out.println();

            iQ = q3;
            // поместить ряд символов в кольцевую очередь
            for (i = 0; i < 10; i++) {
                iQ.put((char) ('A' + i));
            }
            // отобразить содержимое очереди
            System.out.print("Contents of circular queue: ");
            for(i=0; i < 10; i++) {
                ch = iQ.get();
                System.out.print(ch);
            }
            System.out.println();
            // поместить больше символов в кольцевую очередь
            for (i = 10; i < 20; i++) {
                iQ.put((char) ('A' + i));
            }
            // отобразить содержимое очереди
            System.out.print("Contents of circular queue: ");
            for(i=0; i < 10; i++)   {
                ch = iQ.get();
                System.out.print(ch);
            }
            System.out.println("\nStore and consume from" + " circular queue.");
            // поместить символы в кольцевую очередь и извлечь их оттуда
            for (i = 0; i < 20; i++)   {
                iQ.put((char) ('A' + i));
                ch = iQ.get();
                System.out.print(ch);
            }
        } catch(QueueFullException ex) {
            System.out.println(ex.toString());
        } catch(QueueEmptyException ex) {
            System.out.println(ex.toString());
        }
    }
}
