package com.apriorit.shildt.testAppCh7;

public class SupSubRef {
    public static void main(String[] args) {
        Asub asub = new Asub(10);
        Asub asub2;
        Bsub bsub1 = new Bsub(5, 6) ;

        asub2 = asub; // Допустимо, так как обе переменные одного типа.
        System.out.println("asub2.а: " + asub2.a);

        // Класс Y является подклассом X, поэтому переменные х2 и у 1
        // могут ссылаться на один и тот же объект производного класса.
        asub2 = bsub1; // По-прежнему допустимо по указанной выше причине.
        System.out.println("asub2.а: " + asub2.a);

        // В классе X известны только члены класса X.
        asub2.a = 19; // Допустимо.
        //asub2.b=27; // Ошибка, так как переменная Ь не является членом класса X.

    }
}

class Asub {
    int a;
    Asub(int i) {
        a = i;
    }
}

class Bsub extends Asub {
    int b;
    Bsub(int i, int j) {
        super(i);
        b = j;
    }
}