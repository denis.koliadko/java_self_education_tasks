package com.apriorit.shildt.testAppCh7;

public class DynDispDemo {
    public static void main(String args[])  {
        Warrior warrior = new Warrior();
        Axeman axeman1 = new Axeman();
        Swordsman swordsman1 = new Swordsman();

        Warrior person;

        // В каждом из приведенных ниже вызовов конкретный вариант
        // метода fight() выбирается во время выполнения по типу
        // объекта, на который делается ссылка.
        person = warrior;
        person.fight();

        person = axeman1;
        person.fight();
        ((Axeman) person).defend();

        person = swordsman1;
        person.fight();
        ((Swordsman) person).defend();
    }
}

class Warrior {
    void fight()  {
        System.out.print("I am warrior. ");
        System.out.println("My fist will be on your face");
    }
}

class Axeman extends Warrior {
    void fight()  {
        System.out.print("I am viking. ");
        System.out.println("I will fight with AXE");
    }

    void defend() {
        System.out.println("I have light armor and round shield");
    }
}

class Swordsman extends Warrior {
    void fight()  {
        System.out.print("I am palladin. ");
        System.out.println("I will fight with SWORD");
    }
    void defend() {
        System.out.println("I have heavy armor and long shield");
    }
}