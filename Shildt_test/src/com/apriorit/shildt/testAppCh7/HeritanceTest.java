package com.apriorit.shildt.testAppCh7;

public class HeritanceTest {
    HeritanceTest() {
        System.out.println("Parent of all the parents");
    }

    public static void main(String[] args) {
        System.out.println("now let kids do their constructors: ");
        C c = new C();
    }
}

class A extends HeritanceTest {
    A() {
        System.out.println("kidA of the parent");
    }
}

class B extends A {
    B() {
        System.out.println("kidB of the parentA");
    }
}

class C extends B {
    C() {
        System.out.println("kidC of the parentB");
    }
}


