package com.apriorit.shildt.testAppCh7;

import com.apriorit.shildt.testAppCh6.Triangle;
import com.apriorit.shildt.testAppCh6.*;

public class Shapes7 {

    public static void main(String args[])  {
        Triangle tl = new Triangle("right", 8.0, 12.0);

        // создать копию объекта tl
        Triangle t2 = new Triangle(tl);

        System.out.println("Info for tl: ");
        tl.showStyle();
        tl.showDim();
        System.out.println ("Area is " + tl.area());

        System.out.println() ;

        System.out.println("Info for t2: ");
        t2.showStyle();
        t2.showDim();
        System.out.println("Area is " + t2.area() + "\n----------------------------------");

        TwoDShape shapes[] = new TwoDShape[4];
        shapes[0] = new Triangle("right", 8.0, 12.0);
        shapes[1] = new Rectangle(10);
        shapes[2] = new Rectangle(10, 4);
        shapes[3] = new Triangle(7.0);

        for(TwoDShape item : shapes)    {
            System.out.println("object is " + item.getName() + "\nArea is " + item.area() + "\n");
        }
    }


}

// Подкласс, производный от класса TwoDShape,
// для представления прямоугольников.
class Rectangle extends TwoDShape {
    // Конструктор по умолчанию.
    Rectangle() {
        super();
    }

    // Конструктор класса Rectangle.
    Rectangle(double w, double h) {
        super(w, h, "rectangle"); // вызвать конструктор суперкласса
    }

    // построить квадрат
    Rectangle(double х) {
        super(х, "rectangle"); // вызвать конструктор суперкласса
    }

    // построить один объект на основании другого объекта
    Rectangle(Rectangle ob) {
        super(ob); // передать объект конструктору класса TwoDShape
    }

    boolean isSquare() {
        if (getWidth() == getHeight()) return true;
        return false;
    }

    public double area() {
        return getWidth() * getHeight();
    }
}