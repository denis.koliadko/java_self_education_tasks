package com.apriorit.shildt.testAppCh15;

import java.applet.Applet;
import java.awt.*;

public class SimpleApplet extends Applet {
    public void paint(Graphics g) {
        // Вывод символьной строки в окне апплета.
        g.drawString("Java makes applets easy.", 20, 20);
    }
}