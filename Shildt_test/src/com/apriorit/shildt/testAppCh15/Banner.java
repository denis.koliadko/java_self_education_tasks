package com.apriorit.shildt.testAppCh15;

/*
    Пример для опробования 14.1.

    Простой апплет, отображающий крупный заголовок.

    В этом апплете создается поток, управляющий прокруткой
    крупного заголовка, хранящегося в переменной msg,
    справа налево в окне апплета.
*/

import java.awt.*;
import java.applet.*;
/*
    <applet code="Banner" width=300 height=50>
    </applet>
*/

public class Banner extends Applet implements Runnable {
    String msg = " Java Rules the Web "; // "Java правит Паутиной"
    Thread t;
    boolean stopFlag;

    // инициализировать переменную t пустым значением null
    public void init() {
        t = null;
    }

    // запустить поток
    public void start() {
        t = new Thread(this);
        stopFlag = false;
        t.start();
    }

    // Точка входа в поток, манипулирующий крупным заголовком,
    public void run() {
        // отобразить крупный заголовок снова
        for (; ; ) {
            try {
                repaint();
                Thread.sleep(250);
                if (stopFlag)
                    break;
            } catch (InterruptedException exc) {
            }
        }
    }

    // остановить выполнение апплета
    public void stop() {
        stopFlag = true;
        t = null;
    }

    // отобразить крупный заголовок
    public void paint(Graphics g) {
        char ch;
        ch = msg.charAt(0);
        msg = msg.substring(1, msg.length());
        msg += ch;
        g.drawString(msg, 50, 30);
    }
}