package com.apriorit.shildt.testAppCh11;

public class Sync {
    public static void main(String[] args) {
        int a[] = {1, 2, 3, 4, 5};

        new MyThreadSyn("Child #1", a);
        new MyThreadSyn("Child #2", a);
    }
}

class SumArray {
    private int sum;

    synchronized int sumArray(int nums[]) {
        sum = 0;

        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            System.out.println("Running total for " + Thread.currentThread().getName() + " is " + sum);
            try {
                Thread.sleep(10); // чтобы разрешить переключение задач
            } catch (InterruptedException exc) {
                System.out.println("Main thread interrupted.");
            }
        }
        return sum;
    }
}

class MyThreadSyn implements Runnable {
    Thread thrd;
    static SumArray sa = new SumArray();
    int arr[];
    int answer;

    MyThreadSyn(String name, int[] nums) {
        thrd = new Thread(this, name);
        arr = nums;
        thrd.start(); // начать поток
    }

    @Override
    public void run() {
        System.out.println(thrd.getName() + " starting.");
        answer = sa.sumArray(arr);
        System.out.println("Sum for " + thrd.getName() + " is " + answer);
        System.out.println(thrd.getName() + " terminating.");
    }
}
