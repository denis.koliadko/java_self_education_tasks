package com.apriorit.shildt.testAppCh11;

public class Suspend {
    public static void main(String args[]) {
        MyThreadSR obi = new MyThreadSR("My Thread");

        try {
            Thread.sleep(1000); // позволить потоку оЫ начать исполнение

            obi.mySuspend();
            System.out.println("Suspending thread.");
            Thread.sleep(1000);

            obi.myResume();
            System.out.println("Resuming thread.");
            Thread.sleep(1000);

            obi.mySuspend();
            System.out.println("Suspending thread.");
            Thread.sleep(1000);

            obi.myResume();
            System.out.println("Resuming thread.");
            Thread.sleep(1000);

            obi.mySuspend();
            System.out.println("Stopping thread.");
            obi.myStop();
        } catch (InterruptedException e) {
            System.out.println("Main thread Interrupted");
        }
        // ожидать завершения потока
        try {
            obi.thrd.join();
        } catch (InterruptedException e) {
            System.out.println("Main thread Interrupted");
        }
        System.out.println("Main thread exiting.");
    }
}

// Приостановка, возобновление и остановка потока.
class MyThreadSR implements Runnable {
    Thread thrd;

    // Если эта переменная принимает логическое значение
    // true, исполнение потока приостанавливается.
    volatile boolean suspended;
    // Если эта переменная принимает логическое значение
    // true, исполнение потока прекращается.
    volatile boolean stopped;

    MyThreadSR(String name) {
        thrd = new Thread(this, name);
        suspended = false;
        stopped = false;
        thrd.start();
    }

    // Точка входа в поток
    public void run() {
        System.out.println(thrd.getName() + " starting.");
        try {
            for (int i = 1; i < 1000; i++) {
                System.out.print(i + " ");
                if ((i % 10) == 0) {
                    System.out.println();
                    Thread.sleep(250);
                }

                // Для проверки условий приостановки и остановки потока
                // используется следужхций синхронизированный блок.
                synchronized (this) {
                    while (suspended) {
                        wait();
                    }
                    if (stopped) break;
                }
            }
        } catch (InterruptedException exc) {
            System.out.println(thrd.getName() + " interrupted.");
        }
        System.out.println(thrd.getName() + " exiting.");
    }

    // остановить поток
    synchronized void myStop()

    {
        stopped = true;

        // Следующие операторы обеспечивают полную
        // остановку приостановленного потока,
        suspended = false;
        notify();
    }

    // приостановить поток
    synchronized void mySuspend() {
        suspended = true;
    }

    // возобновить поток
    synchronized void myResume() {
        suspended = false;
        notify();
    }
}
