package com.apriorit.shildt.testAppCh11;

public class ExtendThread {
    public static void main(String[] args) {
        System.out.println("Main thread starting.");
        // сначала построить объект типа MyThread
        MyThreadE mt = new MyThreadE("ChildThread #1"); // Создание исполняемого объекта.

        for (int i = 0; i < 50; i++) {
            System.out.print("...");
            try {
                Thread.sleep(100);
            } catch (InterruptedException exc) {
                System.out.println("Main thread interrupted.");
            }
        }
        System.out.println("Main thread ending.");
    }
}

class MyThreadE extends Thread {

    MyThreadE(String name) {
        super(name);
        start();
    }

    // Точка входа в поток,
    @Override
    public void run() {
        // Здесь начинают исполняться потоки.
        System.out.println(this.getName() + " starting.");
        try {
            for (int count = 0; count < 10; count++) {
                Thread.sleep(400);
                System.out.println("In " + this.getName() + ", count is " + count);
            }
        } catch (InterruptedException exc) {
            System.out.println(this.getName() + " interrupted.");
        }
        System.out.println(this.getName() + " terminating.");
    }
}