package com.apriorit.shildt.testAppCh11;

public class SyncE {

    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5};
        MyThreadSynE mtl = new MyThreadSynE("Child #1", nums);
        MyThreadSynE mt2 = new MyThreadSynE("Child #2", nums);

        try {
            mtl.thrd.join();
            mt2.thrd.join();
        } catch (InterruptedException exc) {
            System.out.println("Main thread interrupted.");
        }
    }
}

class SumArrayE {
    private int sum;

    int sumArray(int nums[]) {
        sum = 0;

        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            System.out.println("Running total for " + Thread.currentThread().getName() + " is " + sum);
            try {
                Thread.sleep(10); // чтобы разрешить переключение задач
            } catch (InterruptedException exc) {
                System.out.println("Main thread interrupted.");
            }
        }
        return sum;
    }
}


class MyThreadSynE implements Runnable {
    Thread thrd;
    static SumArrayE sa = new SumArrayE();
    int arr[];
    int answer;

    MyThreadSynE(String name, int[] nums) {
        thrd = new Thread(this, name);
        arr = nums;
        thrd.start();
    }

    @Override
    public void run() {
        synchronized (sa) {
            System.out.println(thrd.getName() + " starting.");
            answer = sa.sumArray(arr);
            System.out.println("Sum for " + thrd.getName() + " is " + answer);
            System.out.println(thrd.getName() + " terminating.");
        }
    }
}

