package com.apriorit.shildt.testAppCh11;

public class JoinThreads {
    public static void main(String[] args) {
        MyThreadR mtr1 = new MyThreadR("Child #1");
        MyThreadR mtr2 = new MyThreadR("Child #2");
        MyThreadR mtr3 = new MyThreadR("Child #3");

        try {
            // Ожидание до тех nop, пока указанный метод не завершится.
            mtr1.thrd.join();
            System.out.println("Child #1 joined.");
            mtr2.thrd.join();
            System.out.println("Child #2 joined.");
            mtr3.thrd.join();
            System.out.println("Child #3 joined.");
        } catch (InterruptedException exc) {
            System.out.println("Main thread interrupted.");
        }
        System.out.println("Main thread ending.");
    }
}
