package com.apriorit.shildt.testAppCh11;

public class UseThreads {
    public static void main(String[] args) {
        System.out.println("Main thread starting.");
        // сначала построить объект типа MyThread
        MyThread mt = new MyThread("ChildThread #1"); // Создание исполняемого объекта.
        // далее сформировать поток из этого объекта
        Thread newThrd = new Thread(mt); // Формирование потока из этого объекта.
        // и, наконец, начать исполнение потока

        newThrd.start(); // Начало исполнения потока.
        for (int i = 0; i < 50; i++) {
            System.out.print("...");
            try {
                Thread.sleep(100);
            } catch (InterruptedException exc) {
                System.out.println("Main thread interrupted.");
            }
        }
        System.out.println("Main thread ending.");
    }
}

// Создание потока путем реализации интерфейса Runnable,
class MyThread implements Runnable {
    Thread thrd;

    // Объекты типа MyThread выполняются в отдельных потоках, так как
    // класс MyThread реализует интерфейс Runnable.
    MyThread(String name) {
        thrd = new Thread(this, name);
    }

    // Точка входа в поток,
    public void run() {
        // Здесь начинают исполняться потоки.
        System.out.println(thrd.getName() + " starting.");
        try {
            for (int count = 0; count < 10; count++) {
                Thread.sleep(400);
                System.out.println("In " + thrd.getName() + ", count is " + count);
            }
        } catch (InterruptedException exc) {
            System.out.println(thrd.getName() + " interrupted.");
        }
        System.out.println(thrd.getName() + " terminating.");
    }
}