package com.apriorit.shildt.testAppCh11;

// Создание нескольких потоков.
class MyThreadR implements Runnable {
    Thread thrd;

    // построить новый поток
    MyThreadR(String name) {
        thrd = new Thread(this, name);
        thrd.start(); // начать поток
    }

    // начать исполнение нового потока
    public void run() {
        System.out.println(thrd.getName() + " starting.");
        try {
            for (int count = 0; count < 10; count++) {
                Thread.sleep(400);
                System.out.println("In " + thrd.getName() + ", count is " + count);
            }
        } catch (InterruptedException exc) {
            System.out.println(thrd.getName() + " interrupted.");
        }
        System.out.println(thrd.getName() + " terminating.");
    }
}

public class MoreThreads {
    public static void main(String args[]) {
        System.out.println("Main thread starting.");

        // Создание и запуск на исполнение трех потоков.
        MyThreadR mtl = new MyThreadR("Child #1");
        MyThreadR mt2 = new MyThreadR("Child #2");
        MyThreadR mt3 = new MyThreadR("Child #3");

        do {
            System.out.print(" . ");
            try {
                Thread.sleep(100);
            } catch (InterruptedException exc) {
                System.out.println("Main thread interrupted.");
            }
            // Ожидание завершения потоков.
        } while (mtl.thrd.isAlive() ||
                mt2.thrd.isAlive() ||
                mt3.thrd.isAlive());
        System.out.println("Main thread ending.");
    }
}

