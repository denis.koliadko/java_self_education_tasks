package com.apriorit.shildt.testAppCh6;

// Простой пример рекурсии,
class Factorial {
    // Рекурсивный метод,
    int factR(int n) {
        int result;

        if(n == 1) return 1;
        // Рекурсивный вызов метода factRO .
        result = factR(n - 1) * n;
        return result;
    }

    // Вариант программы, вычисляющий факториал итеративным способом,
    int factl(int n) {
        int t, result;

        result = 1;
        for(t = 1; t <= n; t++) {
            result *= t;
        }
        return result;
    }
}

class Recursion {
    public static void main(String args[])  {
        Factorial f = new Factorial();

        System.out.println("Factorials using recursive method.");
        long begTime = System.currentTimeMillis();
        System.out.println("Factorial of    3   is  "   +   f.factR(3));
        System.out.println("Factorial of    4   is  "   +   f.factR(4));
        System.out.println("Factorial of    25   is  "   +   f.factR(25));
        long endTime = System.currentTimeMillis();
        String timeToMakeOperation = Utils.msConverter(endTime - begTime);
        System.out.println("Time to get factorial Recursive method: " + timeToMakeOperation);
        System.out.println();

        System.out.println("Factorials using iterative method.");
        begTime = System.currentTimeMillis();
        System.out.println("Factorial of    3   is  "   +   f.factl(3));
        System.out.println("Factorial of    4   is  "   +   f.factl(4));
        System.out.println("Factorial of    25   is  "   +   f.factl(25));
        endTime = System.currentTimeMillis();
        timeToMakeOperation = Utils.msConverter(endTime - begTime);
        System.out.println("Time to get factorial Iterative method: " + timeToMakeOperation);
    }
}
