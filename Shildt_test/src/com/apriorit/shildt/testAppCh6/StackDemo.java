package com.apriorit.shildt.testAppCh6;

import com.apriorit.shildt.testAppCh9.StackEmptyException;
import com.apriorit.shildt.testAppCh9.StackFullException;

public class StackDemo {
    public static void main(String args[]) {
        Stack bigS = new Stack(26);
        Stack smallS = new Stack(4);
        char ch;
        int i;

        try {
            System.out.println("Using bigS to store the alphabet.");
            // поместить буквенные символы в очередь bigS
            for (i = 0; i < 26; i++) {
                bigS.put((char) ('A' + i));
            }

            // извлечь буквенные символы из очереди bigS и отобразить
            System.out.print("Contents of bigS: ");
            for (i = 0; i < 26; i++) {
                ch = bigS.get();
                if (ch != (char) 0) {
                    System.out.print(ch);
                }
            }
            System.out.println("\n");

            System.out.println("Using smallS to generate errors.");
            // использовать небольшую очередь smallS для генерации ошибок
            for (i = 0; i < 5; i++) {
                System.out.print("Attempting to store " + (char) ('Z' - i));
                smallS.put((char) ('Z' - i));
                System.out.println();
            }
            System.out.println();

            // дополнительные ошибки при обращении к очереди smallS
            System.out.print("Contents of smallS: ");
            for (i = 0; i < 5; i++) {
                ch = smallS.get();
                if (ch != (char) 0) System.out.print(ch);
            }
        } catch (StackFullException ex) {
            System.out.println(ex.toString());
        } catch (StackEmptyException ex) {
            System.out.println(ex.toString());
        }
    }
}
