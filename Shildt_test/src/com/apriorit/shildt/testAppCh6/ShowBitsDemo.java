package com.apriorit.shildt.testAppCh6;

// продемонстрировать класс ShowBits
class ShowBitsDemo {
    public static void main(String args[]) {


        System.out.println("123 in binary: ");
        ShowBits.show(123, 8);

        System.out.println("\n8798 in binary: ");
        ShowBits.show(8798, 16);

        System.out.println("\n237658768 in binary: ");
        ShowBits.show(237658768, 32);

        // можно также отобразить младшие разряды любого целого числа
        System.out.println("\nLow order 8 bits of 8797 in binary: ");
        ShowBits.show(87987, 8);
    }
}
