package com.apriorit.shildt.testAppCh6;

import com.apriorit.shildt.testAppCh9.StackEmptyException;
import com.apriorit.shildt.testAppCh9.StackFullException;

public class Stack {
    private char stack[]; // Массив для хранения элементов stack
    private int index; // Индексы размещения и извлечения элементов stack

    public Stack(int size) {
        stack = new char[size];
        index = size - 1;
    }

    void put(char c) throws StackFullException {

        if (index < 0) {
//            System.out.println(" - sorrian, Stack is full");
//            return;
            throw new StackFullException(stack.length);
        }
        stack[index] = c;
        index--;
    }

    char get() throws StackEmptyException {
        if (index == stack.length - 1) {
//            System.out.println(" - sorrian, Stack is empty.");
//            return (char) 0;
            throw new StackEmptyException();
        }
        index++;
        return stack[index];
    }
}
