package com.apriorit.shildt.testAppCh6;

// Отображение битов, составляющих байт,
public class ShowBits {

    public static void show(long val, int numbits) {
        long mask = 1;
        if (numbits < 4) {
            numbits = 4;
        } else if ((numbits % 4) != 0) {
            numbits = (numbits / 4) * 4 + 4;
        }

        // сдвинуть значение 1 влево на нужную позицию
        mask <<= numbits-1;

        int spacer = 0;
        for ( ; mask != 0; mask >>>= 1)    {
            if ((val & mask) != 0) {
                System.out.print('\u2776');
            } else {
                System.out.print("0");
            }
            spacer++;
            if ((spacer % 4) == 0) {
                System.out.print(" ");
                spacer = 0;
            }
        }
        System.out.println();
    }

    public static void main(String args[])  {
        int t;
        int val;

        val = 7;
        for (t = 128; t > 0; t = t/2) {
            if ((val & t) != 0) {
                System.out.print("1 ");
            } else {
                System.out.print("0 ");
            }
        }
    }
}
