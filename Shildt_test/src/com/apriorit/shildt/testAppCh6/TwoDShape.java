package com.apriorit.shildt.testAppCh6;

public abstract class TwoDShape {
    private double width;
    private double height;
    private String name;

    // Конструктор по умолчанию.
    public TwoDShape() {
        width = height = 0.0;
    }

    // Параметризированный конструктор.
    public TwoDShape(double w, double h) {
        width = w;
        height = h;
    }

    // Конструирование объекта с одинаковыми значениями
    // переменных экземпляра width и height.
    public TwoDShape(double x) {
        width = height = x;
    }

    public TwoDShape(TwoDShape t1) {
        width = t1.width;
        height = t1.height;
    }

    // построить объект с одинаковыми значениями
    // переменных экземпляра width и height
    public TwoDShape(double x, String n) {
        width = height = x;
        name = n;
    }

    public TwoDShape(double w, double h, String n) {
        width = w;
        height = h;
        name = n;
    }

    // Методы доступа к переменным экземпляра width и height.
    public double getWidth() { return width; }
    public double getHeight() { return height; }
    public String getName() { return name; }
    public void setWidth(double w) { width = w; }
    public void setHeight(double h) { height = h; }
    public void setName(String n) { name = n; }

    public void showDim()  {
        System.out.println("Width and height are " +
                width + " and " + height);
    }

    public abstract double area();
}