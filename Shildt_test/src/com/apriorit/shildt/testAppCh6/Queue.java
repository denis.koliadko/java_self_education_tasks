package com.apriorit.shildt.testAppCh6;

public class Queue {
    private char queue[]; // Массив для хранения элементов очереди
    private int putLoc, getLoc; // Индексы размещения и извлечения элементов очереди

    public Queue(int size) {
        queue = new char[size + 1];
        putLoc = 0;
        getLoc = 0;
    }

    void put(char c) {
        if (putLoc == (queue.length - 1)) {
            System.out.println(" - sorrian, Queue is full");
            return;
        }
        putLoc++;
        queue[putLoc] = c;
    }

    char get() {
        if(getLoc == putLoc) {
            System.out.println(" - sorrian, Queue is empty.");
            return (char) 0;
        }
        getLoc++;
        return queue[getLoc];
    }
}
