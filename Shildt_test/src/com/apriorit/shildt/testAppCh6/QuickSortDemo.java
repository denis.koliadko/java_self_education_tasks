package com.apriorit.shildt.testAppCh6;

public class QuickSortDemo {
    public static void main(String args[]) {
        //char a[] = { 'd', 'x', 'a', 'r', 'p', 'j', 'i' };
        int i;

        char charsArray[];
        int a, b, t;
        int size;

        size = 10000; // Количество элементов для сортировки

        charsArray = new char[size];
        int range = size * 10;
        for (i = 0; i < size; i++) {
            charsArray[i] = Utils.getRandomChar();
        }

        System.out.print("Original array: ");
        for (i = 0; i < charsArray.length; i++) {
            System.out.print(charsArray[i]);
        }
        System.out.println();

        long begTime = System.currentTimeMillis();
        // отсортировать массив
        Quicksort.qsort(charsArray);
        long endTime = System.currentTimeMillis();

        System.out.print("Sorted array: ");
        for (i = 0; i < charsArray.length; i++) {
            System.out.print(charsArray[i]);
        }
        String timeToSort = Utils.msConverter(endTime - begTime);
        System.out.println("\nTime to sort: " + timeToSort);

    }
}
