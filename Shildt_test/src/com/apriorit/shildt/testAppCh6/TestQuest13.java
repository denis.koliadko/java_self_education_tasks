package com.apriorit.shildt.testAppCh6;

public class TestQuest13 {

    static int sum (int ... n) {
        int result = 0;
        for (int item : n) {
            result += item;
        }
        return result;
    }
}

class SumDemo {
    public static void main(String args[]) {

        int total = TestQuest13.sum(1, 10, 100); //111
        System.out.println("Sum is " + total);
    }
}
