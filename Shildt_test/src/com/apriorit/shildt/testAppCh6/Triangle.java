package com.apriorit.shildt.testAppCh6;

public class Triangle extends TwoDShape {
    private String name;

    // Использование оператора super() для вызова
    // разных вариантов конструктора TwoDShape() .
    // Конструктор по умолчанию.
    Triangle() {
        super(); // вызвать конструктор суперкласса по умолчанию
        name = "null";
    }

    // Параметризированный конструктор.
    public Triangle(String s, double w, double h) {
        super(w, h); // вызвать конструктор суперкласса с двумя аргументами
        name = s;
    }

    // Конструктор с одним аргументом.
    public Triangle(double х) {
        super(х); // вызвать конструктор суперкласса с одним аргументом
        name = "isosceles";
    }

    public Triangle(Triangle tl) {
        super(tl);
        name = tl.getName();
    }

    public String getName() {
        return name;
    }

    public double area() {
        return getWidth() * getHeight() / 2;
    }

    public void showStyle() {
        System.out.println("Triangle is " + name);
    }
}

class Shapes5 {
    public static   void main(String args[]) {
        Triangle tl = new Triangle();
        Triangle t2 = new Triangle("right", 8.0, 12.0);
        Triangle t3 = new Triangle(4.0);

        tl = t2;

        System.out.println("Info for tl: ");
        tl.showStyle();
        tl.showDim();
        System.out.println ("Area is " + tl.area());

        System.out.println() ;

        System.out.println("Info for t2: ");
        t2.showStyle();
        t2.showDim();
        System.out.println("Area is " + t2.area());

        System.out.println();

        System.out.println("Info for t3: ");
        t3.showStyle();
        t3.showDim();
        System.out.println("Area is " + t3.area());

        System.out.println();
    }
}