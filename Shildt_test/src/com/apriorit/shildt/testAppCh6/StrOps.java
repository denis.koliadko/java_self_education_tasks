package com.apriorit.shildt.testAppCh6;

// Некоторые операции над символьными строками,
class StrOps {
    public static void main(String args[]) {
        String strl = "When it comes to Web programming, Java is #1.";
        String str2 = new String(strl);
        String str3 = "Java strings are powerful.";
        int result, idx;

        System.out.println("Length of strl: " + strl.length());

        // отобразить строку strl посимвольно,
        for (int i = 0; i < strl.length(); i++) {
            System.out.print(strl.charAt(i));
        }
        System.out.println();

        if (strl.equals(str2)) {
            System.out.println("strl equals str2");
        } else {
            System.out.println("strl does not equal str2");
        }
        if (strl.equals(str3)) {
            System.out.println("strl equals str3");
        } else {
            System.out.println("strl does not equal str3");
        }
        result = strl.compareTo(str3);
        if (result == 0) {
            System.out.println("strl and str3 are equal");
        } else if (result < 0) {
            System.out.println("strl is less than str3");
        } else {
            System.out.println("strl is greater than str3");
        }

        // присвоить переменной str2 новую строку
        str2 = "One Two Three One";

        idx = str2.indexOf("One");
        System.out.println("Index of first occurrence of One: " + idx);
        idx = str2.lastIndexOf("One");
        System.out.println("Index of last occurrence of One: " + idx);
    }
}
