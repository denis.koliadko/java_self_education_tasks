package com.apriorit.shildt.testAppCh6;

public class App {
    public static void main(String args[]) {
        int nums[];
        int a, b, t;
        int size;

        size = 10000; // Количество элементов для сортировки

        nums = new int[size];
        int range = size * 10;
        for (int i = 0; i < size; i++) {
            nums[i] = Utils.getRandomNumberInRange(0, range);
        }

        // отобразить исходный массив
        System.out.print("Original array is:");
        for(int i = 0; i < size; i++) {
            System.out.print(" " + nums[i]);
        }
        System.out.println();

        long begTime = System.currentTimeMillis();
        // реализовать алгоритм пузырьковой сортировки
        for (a = 1; a < size; a++) {
            for (b = size - 1; b >= a; b--) {
                if(nums[b - 1] > nums[b]) { // если требуемый порядок
                    // следования не соблюдается, поменять элементы местами
                    t = nums[b - 1];
                    nums[b - 1] = nums[b];
                    nums[b] = t;
                }
            }
        }
        long endTime = System.currentTimeMillis();
        // отобразить отсортированный массив
        System.out.print ("Sorted array is: ");
        for(int i = 0; i < size; i++) {
            System.out.print(" " + nums[i]);
        }
        System.out.println();
        String timeToSort = Utils.msConverter(endTime - begTime);

        System.out.println("Time to sort: " + timeToSort);
    }
}
