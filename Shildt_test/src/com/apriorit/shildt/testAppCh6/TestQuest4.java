package com.apriorit.shildt.testAppCh6;

public class TestQuest4 {
    public static void main(String[] args) {

        Test ob1 = new Test(10);
        Test ob2 = new Test(100);

        TestQuest4 t = new TestQuest4();
        t.swap(ob1, ob2);

        System.out.println("ob1.a = " + ob1.a);
        System.out.println("ob2.a = " + ob2.a);

    }

    void swap (Test ob1, Test ob2) {
        int temp = ob1.a;
        ob1.a = ob2.a;
        ob2.a = temp;
    }

    static class Test {
        int a;
        Test (int i) {
            this.a = i;
        }
    }
}
