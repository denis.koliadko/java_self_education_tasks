package com.apriorit.shildt.testAppCh6;

// Использование операции поразрядного исключающего ИЛИ
// для шифрования и дешифрования сообщений
class Encode {
    public static void main(String args[]) {
        String msg = "This is a test";
        String encmsg = "";
        String decmsg = "";
        String key = "1234";

        System.out.print("Original message: ");
        System.out.println(msg);

        // зашифровать сообщение
        int j = 0;
        for (int i = 0; i < msg.length(); i++) {
            // Построение зашифрованной строки сообщения,
            encmsg = encmsg + (char) (msg.charAt(i) ^ key.charAt(j));
            j =  (j > 3) ?  0 : j++;
        }
        System.out.print("Encoded message: ");
        System.out.println(encmsg) ;

        j = 0;
        // дешифровать сообщение
        for (int i = 0; i < msg.length(); i++) {
            // Построение дешифрованной строки сообщения.
            decmsg = decmsg + (char) (encmsg.charAt(i) ^ key.charAt(j));
            j =  (j > 3) ?  0 : j++;
        }
        System.out.print("Decoded message: ");
        System.out.println(decmsg);
    }
}