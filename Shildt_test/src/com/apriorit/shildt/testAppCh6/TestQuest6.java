package com.apriorit.shildt.testAppCh6;

public class TestQuest6 {
    public static void main(String[] args) {
        TestQuest6 t = new TestQuest6();
        String s = "Test string";
        t.recursiveStringBackwards(0, s);
    }
    void recursiveStringBackwards(int index, String str) {
        if (index >= str.length()) {
            return;
        } else {
            recursiveStringBackwards(index + 1, str);
            System.out.print(str.charAt(index));
        }

    }
}
