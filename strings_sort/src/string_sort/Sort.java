package string_sort;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Sort {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int numberOfStrings = 0;

        System.out.println("Input number of strings");
        try {
            numberOfStrings = scan.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Wrong data");
            System.exit(1);
        }
        System.out.println("Input strings:");

        String[] arrayOfStrings = new String[numberOfStrings];
        for (int i = 0; i < numberOfStrings; i++ ) {
            arrayOfStrings[i] = scan.next();
        }
        sortStrings(arrayOfStrings);

        System.out.println("Sorted strings:");
        printSortedStrings(arrayOfStrings);
    }

    private static void printSortedStrings(String[] arrayOfStrings) {
        for (String item : arrayOfStrings) {
            System.out.println(item);
        }
    }

    private static void sortStrings(String[] arrayOfStrings) {
        String temp;
        for (int i = 1; i < arrayOfStrings.length; i++) {
            for (int j = arrayOfStrings.length - 1; j >= i; j--) {
                if (arrayOfStrings[j - 1].compareTo(arrayOfStrings[j]) > 0) {
                    temp = arrayOfStrings[j - 1];
                    arrayOfStrings[j - 1] = arrayOfStrings[j];
                    arrayOfStrings[j] = temp;
                }
            }
        }
    }
}
