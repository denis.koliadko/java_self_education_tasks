package demo;

public class Application {

	public static void main(String[] args) {
				
		if ((args == null) || (args.length == 0)) System.out.println("Error");
		else {
			switch (args[0]) {
			case "help":
				if (args.length > 1) {
					System.out.println("Error");
					break;
				} else {
					(new HelpCommand("")).execute();
					break;
				}
			case "echo":
				if (args.length != 2) {
					System.out.println("Error");
					break;
				} else {
					(new EchoCommand(args[1])).execute();
				break;
				}
			case "date":
				if (args.length != 2) {
					System.out.println("Error");
					break;
				} else {
					if (!args[1].equals("now")) {
						System.out.println("Error");
					break;
					} else {
						(new DateCommand("")).execute();
						break;
					}
				}
			case "exit":
				if (args.length > 1) {
					System.out.println("Error");
					break;
				} else {
					(new ExitCommand("")).execute();
					break;
				}
			default:
	        System.out.println("Error");				
			}	
		}		
	}
}
