package demo;

public class EchoCommand implements Command {
	private String value;
	
	public EchoCommand(String value) {		
		this.value = value;
	}

	public void execute() {
		System.out.println(value);		
	}	
}
