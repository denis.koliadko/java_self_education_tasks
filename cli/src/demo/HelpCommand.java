package demo;

public class HelpCommand implements Command {
	private String value;

	public HelpCommand(String value) {		
		this.value = value;
	}

	public void execute() {		
		System.out.println("Help executed");
	}
}
