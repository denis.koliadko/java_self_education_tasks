package demo;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateCommand implements Command {
	private String value;

	public DateCommand(String value) {
		this.value = value;
	}

	public void execute() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss");
		System.out.println(format.format(cal.getTime()));
	}
}
