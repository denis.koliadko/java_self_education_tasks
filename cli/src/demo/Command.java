package demo;

public interface Command {
	void execute();
}
