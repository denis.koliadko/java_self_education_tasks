package demo;

public class ExitCommand implements Command {
	private String value;

	public ExitCommand(String value) {		
		this.value = value;
	}

	public void execute() {		
		System.out.println("Goodbye!");		
	}
}
