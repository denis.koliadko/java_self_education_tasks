package average;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Average {
    private static final String AVERAGE = "average";
    private static final String EXIT = "exit";
    private static List<Double> doubles = new LinkedList<>();

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Input several doubles");
        System.out.println("Type \"average\" to get average");
        System.out.println("Type \"exit\" to exit");

        String inputData;
        boolean notExit = true;

        while (notExit) {
            inputData = scan.nextLine();
            if (inputData.equals(EXIT)) {
                notExit = false;
            } else if (inputData.equals(AVERAGE)) {
                System.out.printf("The average of data is: %.2f\n", doAverage());
                doubles.clear();
            } else {
                try {
                    Double doubleNumber = Double.parseDouble(inputData);
                    doubles.add(doubleNumber);
                } catch (NumberFormatException e) {
                    System.out.println("Illegal input data");
                }
            }
        }
        scan.close();
    }

    private static double doAverage() {
        double average = 0;
        if (!doubles.isEmpty()) {
            for(double item : doubles) {
                average+=item;
            }
            average /= doubles.size();
        }
        return average;
    }
}
